/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */


package org.wmmnpr.c2;


public interface PM3Constants {
	
	public final static String  TKCMDSET_PM3_PRODUCT_NAME  		= "Concept II PM3\0";
	public final static String  TKCMDSET_PM3_PRODUCT_NAME2  	= "Concept2 Performance Monitor 3 (PM3)\0";
	public final static String  TKCMDSET_PM3TESTER_PRODUCT_NAME = "Concept 2 PM3 Tester";
	public final static String  TKCMDSET_PM4_PRODUCT_NAME 		= "Concept2 Performance Monitor 4 (PM4)\0";
	
		
	public final static int CSAFE_PM_SET_SPLITDURATION		= 0x05;
	
	public final static int CSAFE_SETHORIZONTAL_CMD			= 0x21;
	public final static int CSAFE_SETPROGRAM_CMD			= 0x24;
	public final static int CSAFE_SETUSERCFG1_CMD 			= 0x1A;
	public final static int CSAFE_PM_GET_WORKTIME			= 0xA0;
	public final static int CSAFE_PM_GET_WORKDISTANCE		= 0xA3;    
	
	public final static int CSAFE_RESET_CMD					= 0x81;
	public final static int CSAFE_GOIDLE_CMD				= 0x82;
	public final static int CSAFE_GOHAVEID_CMD				= 0x83;
	public final static int CSAFE_GOINUSE_CMD				= 0x85;
	public final static int CSAFE_GOFINISHED_CMD			= 0x86;
	public final static int CSAFE_GOREADY_CMD				= 0x87;	
	

}
