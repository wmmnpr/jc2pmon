/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */

package org.wmmnpr.c2;


/*
 * Exception class to store PM3 Error information
 */
public class PM3Exception extends Exception {
	
	private short ecode = 0;
	
	private String errorName;
		
	private String errorText;

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
	
	
	public PM3Exception(short ecode){		
		this.ecode = ecode;
	}


}
