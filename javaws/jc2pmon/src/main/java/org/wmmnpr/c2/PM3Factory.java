/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */

package org.wmmnpr.c2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wmmnpr.c2.csafe.PM3CsafeCP;
import org.wmmnpr.c2.csafe.impl.PM3CsafeCPImpl;
import org.wmmnpr.c2.ddi.PM3DDICP;
import org.wmmnpr.c2.ddi.impl.PM3DDICPImpl;
import org.wmmnpr.c2.usb.PM3USBCP;
import org.wmmnpr.c2.usb.impl.PM3USBCPImpl;

public class PM3Factory {
	
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PM3Factory.class);
	
    static {
    	slf4jLogger.debug("PM3CsafeCP_JNI loading started.");		
        System.loadLibrary("PM3CsafeCP_JNI");
        
    	slf4jLogger.debug("PM3DDICP_JNI loading started.");        
        System.loadLibrary("PM3DDICP_JNI");      
        
        slf4jLogger.debug("libraries loading done.");
    }
    	
	public static PM3CsafeCP createPM3CsafeCP(){
		return new PM3CsafeCPImpl();
	}

	public static PM3DDICP createPM3DDICP(){
		return new PM3DDICPImpl();
	}
	
	public static PM3USBCP createPM3USBCP(){
		return new PM3USBCPImpl();
	}
		
}
