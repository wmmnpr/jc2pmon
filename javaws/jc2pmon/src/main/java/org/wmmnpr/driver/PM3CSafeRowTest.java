/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */

package org.wmmnpr.driver;


import static org.wmmnpr.c2.PM3Constants.*;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Calendar;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wmmnpr.c2.PM3Exception;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PM3CSafeRowTest extends PM3TestBase {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(PM3CSafeRowTest.class);
	
	private int rsp_data[] 			= new int[RSP_DATA_SIZE];
	private short rsp_data_size[] 	= {RSP_DATA_SIZE};	
	private short unit_address 		= 0;

	/*
	 * Only Test method to be called by Junit. See base class for initialization.
	 */
	@Test
	public void test() throws Exception {		
		showWelcome();
		setupWorkOut();				
		monitorWorkout();	
	}
	
	/*
	 * Show user test message.
	 */	
	private void showWelcome(){
		
		ByteBuffer cmd_ptr = ByteBuffer.wrap("Welcome, please wait.".getBytes());
		
		//pm3ddicp.tkcmdsetDDI_echo(unit_address, cmd_ptr, (short)cmd_ptr.limit());
		
		//pause 
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}			
			
	}
	
	
	/*
	 * set up workout for test.
	 */
	private void setupWorkOut() throws Exception{
		slf4jLogger.info(String.format("Started setup workout"));		
		int cmd_data[][] = {
			{CSAFE_GOFINISHED_CMD},			
			{CSAFE_GOREADY_CMD},
			{CSAFE_SETHORIZONTAL_CMD, 0x03, 0x01, 0x00, 0x22},
			{CSAFE_SETUSERCFG1_CMD, 0x07, CSAFE_PM_SET_SPLITDURATION, 0x05, 0x80, 0x64, 0x00, 0x00, 0x00},
			{CSAFE_SETPROGRAM_CMD, 0x02, 0x00, 0x00},
			{CSAFE_GOIDLE_CMD},
			{CSAFE_GOHAVEID_CMD}, 
			{CSAFE_GOINUSE_CMD}					
		};
		
			
		for(int cidx=0; cidx < cmd_data.length; cidx++){
				
			slf4jLogger.info(String.format("Executing setup command %X", cmd_data[cidx][0]));
							
			//execute command
			sendCommand(unit_address, cmd_data[cidx], rsp_data, rsp_data_size);
			
			printResponse(rsp_data, rsp_data_size);				
			
			//pause 
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			 													
			slf4jLogger.info(String.format("Finished setup workout"));
			
		}	
		
	}
	
	/*
	 * Monitor fixed workout distance configured in setupWorkout until it's 
	 * done or 2 minutes have expired.
	 */
	private void monitorWorkout() throws Exception{
		
		slf4jLogger.info(String.format("Started monitoring workout."));
		
		int cmd_data[] = {
				CSAFE_SETUSERCFG1_CMD, 2, CSAFE_PM_GET_WORKTIME, CSAFE_PM_GET_WORKDISTANCE		
		};
		
		Long start = Calendar.getInstance().getTimeInMillis();
								
		while(true){
				
			//get workout information
			sendCommand(unit_address, cmd_data, rsp_data, rsp_data_size);
			
			//print it to output
			printResponse(rsp_data, rsp_data_size);		
			
			//response
			//[1A:0E:A0:05:00:00:00:00:00:A3:05:E8:03:00:00:00]
			float distance = (rsp_data[11]  + rsp_data[12] * 256 + rsp_data[13] * 65536 + rsp_data[14] * 16777216) * 10 + rsp_data[15];
			distance /= 100.0;
			slf4jLogger.info(String.format("distance remaining %6.1f", distance));	
			
			if(distance <= 0){
				slf4jLogger.info(String.format("Goodbye: workout finished"));	
				break;
			}
							
			//if 1 minute exceeded, then quite.
			if(Calendar.getInstance().getTimeInMillis() - start > (60 * 1000)){
				slf4jLogger.info(String.format("Goodbye: workout time of 1 minute execeeded."));
				break;
			}
								
			//pause 
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}				
			 				
		}
		
		slf4jLogger.info(String.format("Finished monitoring workout."));		
		
	}
	
	/*
	 * print response buffer
	 */
	private void printResponse(int rsp_data[], short rsp_data_size[]){
		
		StringBuilder stringBuilder;
		stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("%04d byte(s)[", rsp_data_size[0]));
		if (rsp_data_size[0] > 0) {
			int i = 0;
			for (i = 0; i < rsp_data_size[0]; i++) {
				stringBuilder.append(String.format("%02X%s", rsp_data[i], 								
						(i < rsp_data_size[0] - 1 ? ":" : "")));
			}
		
		}
		stringBuilder.append("]");
		//print result
		slf4jLogger.info(stringBuilder.toString());		
	}

	/*
	 * call command to PM
	 */
	private void sendCommand(short unit_address, int cmd_data[],
			int rsp_data[], short rsp_data_size[]) throws PM3Exception {

		short ecode = 0;
		IntBuffer cmdDataBuffer = IntBuffer.wrap(cmd_data);
		short cmd_data_size = (short) cmdDataBuffer.limit();

		IntBuffer rspDataBuffer = IntBuffer.wrap(rsp_data);

		rsp_data_size[0] = RSP_DATA_SIZE;
		ShortBuffer rspDataSizeBuffer = ShortBuffer.wrap(rsp_data_size);

		rspDataSizeBuffer.put(0, (short) RSP_DATA_SIZE);
		ecode = pm3CsafeCP.tkcmdsetCSAFE_command(unit_address, cmd_data_size,
				cmdDataBuffer, rspDataSizeBuffer, rspDataBuffer);

		if (ecode != 0) {
			PM3Exception pm3Exception = createException(ecode);
			throw pm3Exception;
		}

	}
	
	public static void main(String args[]){		
		 org.junit.runner.JUnitCore.main("org.wmmnpr.test.PM3CSafeRowTest");	
	}

}
