/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */

package org.wmmnpr.driver;

import java.awt.Toolkit;

import javax.swing.JTextArea;

import ch.qos.logback.core.UnsynchronizedAppenderBase;

public class JTextAreaAppender  extends UnsynchronizedAppenderBase<Object> {
	
	public JTextArea jTextArea;
	
	public JTextAreaAppender(){		
		jTextArea = Main.getLog();	
		//this.start();
	}

	@Override
	protected void append(final Object eventObject) {	
		
		String msg = eventObject.toString();
		
		//PMLogEvent theEvent = new PMLogEvent(this, msg);
		//Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(theEvent);
	
		jTextArea.append(msg + "\n");
		jTextArea.setCaretPosition(jTextArea.getDocument().getLength());
	}
	

}
