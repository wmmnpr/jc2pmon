/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */

package org.wmmnpr.driver;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wmmnpr.c2.PM3Exception;
import org.wmmnpr.c2.PM3Factory;
import org.wmmnpr.c2.csafe.PM3CsafeCP;
import org.wmmnpr.c2.ddi.PM3DDICP;

import static org.wmmnpr.c2.PM3Constants.*;

public class PM3TestBase {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PM3TestBase.class);
	
	public final static  short  RSP_DATA_SIZE 				= 64;	
	
	protected PM3DDICP 		pm3ddicp = null;
	protected PM3CsafeCP 	pm3CsafeCP = null;
	
	protected int numberPmsOnline = -1;
	
	@Before
	public void setUp(){
		 short ecode;
		
		 pm3ddicp = PM3Factory.createPM3DDICP();
		 assertNotNull(pm3ddicp);
		 
		 pm3CsafeCP = PM3Factory.createPM3CsafeCP(); 
		 assertNotNull(pm3CsafeCP);
		 
		ecode = pm3ddicp.tkcmdsetDDI_init();
		if (ecode != 0) {
			slf4jLogger.info(String.format("tkcmdsetDDI_init failed %d", ecode));
		}	
		assertTrue(ecode == 0);
		
		
		int numOldPM3Devices = 0;
		int numNewPM3Devices = 0;
		int numPM4Devices = 0;
		
		// Look for PM3 devices
		short num_communicating[] = new short[1];
		num_communicating[0] = 0;
		short startAddress = 0;
		byte toWrap[] = TKCMDSET_PM3_PRODUCT_NAME2.getBytes();
		ByteBuffer byteBuffer = ByteBuffer.wrap(toWrap);
		
		ShortBuffer numCommunicatingBuffer = ShortBuffer
				.wrap(num_communicating);
		
		// ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(toWrap, 0, startAddress,
		// num_communicating, 0);
		ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(byteBuffer, startAddress,
				numCommunicatingBuffer);
		if (ecode == 0 && numCommunicatingBuffer.get(0) > 0) {
			// We discovered one or more PM3's
			numNewPM3Devices = numCommunicatingBuffer.get(0);
		
		} 
		slf4jLogger.info(String.format("Number of %s is: %d",
				TKCMDSET_PM3_PRODUCT_NAME2, numNewPM3Devices));
		
		// Look for old style PM3 devices, starting numbering after the previous
		byteBuffer = ByteBuffer.wrap(TKCMDSET_PM3_PRODUCT_NAME.getBytes());
		ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(byteBuffer,
				numCommunicatingBuffer.get(0), numCommunicatingBuffer);
		
		if (ecode == 0 && numCommunicatingBuffer.get(0) > 0) {
			// We discovered one or more old PM3's
			numOldPM3Devices = numCommunicatingBuffer.get(0) - numNewPM3Devices;
		}
		slf4jLogger.info(String.format("Number of %s is: %d",
				TKCMDSET_PM3_PRODUCT_NAME, numOldPM3Devices));
		
		// Look for PM4 devices
		byteBuffer = ByteBuffer.wrap(TKCMDSET_PM4_PRODUCT_NAME.getBytes());
		ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(byteBuffer,
				numCommunicatingBuffer.get(0), numCommunicatingBuffer);
		if (ecode == 0 && numCommunicatingBuffer.get(0) > 0) {
			// We discovered one or more PM4's
			numPM4Devices = numCommunicatingBuffer.get(0) - numNewPM3Devices
					- numOldPM3Devices;
		}
		slf4jLogger.info(String.format("Number of %s is: %d",
				TKCMDSET_PM4_PRODUCT_NAME, numPM4Devices));
		
		numberPmsOnline = numCommunicatingBuffer.get(0);
			
		if(numberPmsOnline > 0){
			slf4jLogger.info(String.format("PM's discovered: %s\n", numberPmsOnline));			
		}else{
			slf4jLogger.error(String.format("No PMs found"));		
		}
				
		//
		assertTrue("number of PMs greater than 0", numberPmsOnline > 0);
		
		short timeout = 1000;
		ecode = pm3CsafeCP.tkcmdsetCSAFE_init_protocol(timeout);
		if (ecode != 0) {
			slf4jLogger.info("tkcmdsetCSAFE_init_protocol error");
			// printError(ecode);
		
			assertTrue(false);
		} else {
			slf4jLogger.info("tkcmdsetCSAFE_init_protocol successful\n");
		}	   	 
 
	   	 
	}
	
	
	@After
	public void tearDown(){
		if(pm3ddicp != null)
		pm3ddicp.tkcmdsetDDI_shutdown((short) 1);
	}
	
	protected PM3Exception createException(short ecode){
        
		PM3Exception pm3Exception = new PM3Exception(ecode);
		
    	byte buffer1[] = new byte[1024];
    	ByteBuffer byteBuffer1 = ByteBuffer.wrap(buffer1);
    	// Format the error by looking up info in INI file
    	pm3ddicp.tkcmdsetDDI_get_error_name(ecode, byteBuffer1, (short)1024);
    	pm3Exception.setErrorName(new String(byteBuffer1.array()));
    	  	
    	byte buffer2[] = new byte[512];
    	ByteBuffer byteBuffer2 = ByteBuffer.wrap(buffer2);   	
    	pm3ddicp.tkcmdsetDDI_get_error_text(ecode,byteBuffer2, (short)1024);
    	pm3Exception.setErrorText(new String(byteBuffer2.array()));
    		
    	return pm3Exception;
    	
    }	

}
