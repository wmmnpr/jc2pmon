package org.wmmnpr.driver;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Main extends JPanel implements ActionListener /*, AWTEventListener */ {

	static private final String newline = "\n";
	private JButton runButton;
	private static JTextArea log;
	private JFileChooser fc;
	private File selectedFile = null;
		    
	    
	public static JTextArea getLog() {
		return log;
	}



	public Main() {
	      	super(new BorderLayout());
	      		      	
	        log = new JTextArea(5,20);
	        log.setMargin(new Insets(5,5,5,5));
	        log.setEditable(false);
	        JScrollPane logScrollPane = new JScrollPane(log);

	        runButton = new JButton("Start");
	        runButton.addActionListener(this);

	        JPanel buttonPanel = new JPanel(); 
	        buttonPanel.add(runButton);
	        add(buttonPanel, BorderLayout.PAGE_START);
	        
	                       
	        add(logScrollPane, BorderLayout.CENTER);	
	        
	        //Toolkit.getDefaultToolkit().addAWTEventListener(this, PMLogEvent.id);
	        
		
	}

	private static void createAndShowGUI() {
  
		JFrame frame = new JFrame("PM3CsafeRun");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 	
      	Main main = new Main();    	
      	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
      	
      	main.setPreferredSize(new Dimension(dim.width/2, dim.height/2));
      	     	
		frame.add(main);
		
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String args[]) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}


    public void actionPerformed(ActionEvent e) {
    	if (e.getSource() == runButton) {      	
    		runButton.setEnabled(false);
    		new Thread(new Runnable() {
    			public void run() {
    				try{			
    					org.junit.runner.JUnitCore.runClasses(PM3CSafeRowTest.class);
    					runButton.setEnabled(true);
    				}catch(Exception ex){
    					ex.printStackTrace();
    				}
    			}
    		}).start();    		    		
        }
    }



    /*
	@Override
	public void eventDispatched(AWTEvent e) {  	
		System.out.println(e.getID());
    	if(e.getID() == PMLogEvent.id){
    		log.append(((PMLogEvent)e).getMsg() + "\n");
    		log.setCaretPosition(log.getDocument().getLength());   		
    	}
	}
	*/
}


