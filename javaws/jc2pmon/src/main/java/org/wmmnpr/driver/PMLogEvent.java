package org.wmmnpr.driver;

import java.awt.AWTEvent;
import java.awt.Event;
import java.util.EventObject;

public class PMLogEvent extends AWTEvent {

	public static final int id = AWTEvent.RESERVED_ID_MAX + 1;
	
	public String msg; 
	
	public PMLogEvent(Object source, String msg) {
		super(source, id);
		this.msg = msg;
		
		
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
		
}