/*
 * Created on 08.03.2014
 * wmmnpr@gmail.com
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.0
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'jc2pmon'.
 * The Initial Developer of the Original Code is William Pennoyer.
 * Copyright (C) 2014, 2015, 2016, 2017.
 * All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 */
package org.wmmnpr.junit;

/*
Example output 2014.07.28
20...4:08,197 ...Factory - PM3CsafeCP_JNI loading started.
20...4:08,203 ...Factory - PM3DDICP_JNI loading started.
20...4:08,204 ...Factory - libraries loading done.
20...4:08,240 ...Test - Number of Concept2 Performance Monitor 3 (PM3)NULis: 0
20...4:08,244 ...Test - Number of Concept II PM3 NUL is: 0
20...4:08,248 ...Test - Number of Concept2 Performance Monitor 4 (PM4)NUL is: 1
20...4:08,248 ...Test - PM's discovered: 1
20...4:08,248 ...Test - tkcmdsetCSAFE_init_protocol successful
20...4:08,258 ...Test - 0000 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:10,268 ...Test - 0001 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:12,284 ...Test - 0002 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:14,300 ...Test - 0003 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:16,316 ...Test - 0004 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:18,329 ...Test - 0005 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:20,345 ...Test - 0006 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:22,361 ...Test - 0007 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:24,377 ...Test - 0008 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:26,393 ...Test - 0009 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:20:4E:00:00:00]
20...4:28,407 ...Test - 0010 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:30,423 ...Test - 0011 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:32,439 ...Test - 0012 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:34,455 ...Test - 0013 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:00:00:00:00:00]
20...4:36,471 ...Test - 0014 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:A0:86:01:00:00]
20...4:38,487 ...Test - 0015 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:A0:86:01:00:00]
20...4:40,500 ...Test - 0016 : 16 [1A:0E:A0:05:00:00:00:00:00:A3:05:A0:86:01:00:00]
20...4:42,518 ...Test - 0017 : 16 [1A:0E:A0:05:00:00:00:00:06:A3:05:96:86:01:00:09]
20...4:44,533 ...Test - 0018 : 16 [1A:0E:A0:05:C8:00:00:00:0A:A3:05:78:86:01:00:05]
20...4:46,549 ...Test - 0019 : 16 [1A:0E:A0:05:90:01:00:00:0C:A3:05:5A:86:01:00:06]
20...4:48,565 ...Test - 0020 : 16 [1A:0E:A0:05:58:02:00:00:0F:A3:05:3C:86:01:00:00]
20...4:50,581 ...Test - 0021 : 16 [1A:0E:A0:05:20:03:00:00:0F:A3:05:14:86:01:00:06]
20...4:52,594 ...Test - 0022 : 16 [1A:0E:A0:05:E8:03:00:00:0E:A3:05:F6:85:01:00:07]
20...4:54,610 ...Test - 0023 : 16 [1A:0E:A0:05:B0:04:00:00:12:A3:05:D8:85:01:00:02]
20...4:56,626 ...Test - 0024 : 16 [1A:0E:A0:05:78:05:00:00:11:A3:05:B0:85:01:00:06]
20...4:58,642 ...Test - 0025 : 16 [1A:0E:A0:05:40:06:00:00:13:A3:05:92:85:01:00:04]
 */

import static org.wmmnpr.c2.PM3Constants.*;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.wmmnpr.c2.PM3Factory;
import org.wmmnpr.c2.csafe.PM3CsafeCP;
import org.wmmnpr.c2.ddi.PM3DDICP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

@Ignore
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PM3CsafeTest {
	
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PM3CsafeTest.class);

	public final static  short  RSP_DATA_SIZE 				= 64;	
	
	private PM3DDICP 	pm3ddicp = null;
	private PM3CsafeCP 	pm3CsafeCP = null;
	
	private int numberPmsOnline = -1;
		
	@Before
	public void setUp(){
	   	 pm3ddicp = PM3Factory.createPM3DDICP();
	   	 assertNotNull(pm3ddicp);
	   	 
	   	 pm3CsafeCP = PM3Factory.createPM3CsafeCP(); 
	   	 assertNotNull(pm3CsafeCP);
	}
	
	@Test
	public void AAA_DDI_init() {				
		short ecode;
		
		ecode = pm3ddicp.tkcmdsetDDI_init();	
		if(ecode != 0){
			slf4jLogger.info(String.format("tkcmdsetDDI_init failed %d", ecode));
		}		
		
		assertTrue(ecode == 0);
  	 	
	   int numOldPM3Devices = 0;
	   int numNewPM3Devices = 0;
	   int numPM4Devices = 0;
	   
	      
	   // Look for PM3 devices
	   short num_communicating [] = new short[1];
	   num_communicating[0]=0;
	   short startAddress = 0;
	   byte toWrap[] = TKCMDSET_PM3_PRODUCT_NAME2.getBytes();
	   ByteBuffer byteBuffer = ByteBuffer.wrap(toWrap);
	   	 
	   ShortBuffer numCommunicatingBuffer = ShortBuffer.wrap(num_communicating);
		      	      	   
	   //ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(toWrap, 0, startAddress, num_communicating, 0);
	   ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(byteBuffer, startAddress, numCommunicatingBuffer);  
	   if (ecode == 0  && numCommunicatingBuffer.get(0) > 0)
	   {
	      // We discovered one or more PM3's
	      numNewPM3Devices = numCommunicatingBuffer.get(0);

	   }
	   slf4jLogger.info(String.format("Number of %s is: %d", TKCMDSET_PM3_PRODUCT_NAME2, numNewPM3Devices));	
	   
	   // Look for old style PM3 devices, starting numbering after the previous
	   //
	   byteBuffer = ByteBuffer.wrap(TKCMDSET_PM3_PRODUCT_NAME.getBytes());  	   
	   ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(byteBuffer, numCommunicatingBuffer.get(0), numCommunicatingBuffer);
	  
	   if (ecode == 0 && numCommunicatingBuffer.get(0) > 0)
	   {
	      // We discovered one or more old PM3's
	      numOldPM3Devices = numCommunicatingBuffer.get(0) - numNewPM3Devices;
	   }
	   slf4jLogger.info(String.format("Number of %s is: %d", TKCMDSET_PM3_PRODUCT_NAME, numOldPM3Devices));
	   
	   // Look for PM4 devices
	   byteBuffer = ByteBuffer.wrap(TKCMDSET_PM4_PRODUCT_NAME.getBytes()); 
	   ecode = pm3ddicp.tkcmdsetDDI_discover_pm3s(byteBuffer, numCommunicatingBuffer.get(0), numCommunicatingBuffer);
	   if (ecode == 0 && numCommunicatingBuffer.get(0) > 0)
	   {
	      // We discovered one or more PM4's
	      numPM4Devices = numCommunicatingBuffer.get(0) - numNewPM3Devices - numOldPM3Devices;
	   }
	   slf4jLogger.info(String.format("Number of %s is: %d", TKCMDSET_PM4_PRODUCT_NAME, numPM4Devices));	

	   numberPmsOnline = numCommunicatingBuffer.get(0);

	   slf4jLogger.info(String.format("PM's discovered: %s\n",numberPmsOnline));
	   	   
	   //
	   assertTrue(numberPmsOnline > 0);
		    
	    short timeout = 1000;
		ecode = pm3CsafeCP.tkcmdsetCSAFE_init_protocol(timeout);
		if(ecode != 0){
			slf4jLogger.info("tkcmdsetCSAFE_init_protocol error");
			printError(ecode);
			pm3ddicp.tkcmdsetDDI_shutdown((short) 1);
			assertTrue(false);
		}else{
			slf4jLogger.info("tkcmdsetCSAFE_init_protocol successful\n");
		}		
			
		
		//now the access the
		
		short unit_address = 0;
		
		int cmd_data[] = {CSAFE_SETUSERCFG1_CMD, 2, CSAFE_PM_GET_WORKTIME, CSAFE_PM_GET_WORKDISTANCE};		
		IntBuffer cmdDataBuffer = IntBuffer.wrap(cmd_data);
	
		short cmd_data_size = (short) cmdDataBuffer.limit();
			
		int rsp_data[] = new int[RSP_DATA_SIZE];
		IntBuffer rspDataBuffer = IntBuffer.wrap(rsp_data);
		short rsp_data_size[] = new short[1];
		rsp_data_size[0]= RSP_DATA_SIZE;
		ShortBuffer rspDataSizeBuffer = ShortBuffer.wrap(rsp_data_size);
		
		StringBuilder stringBuilder;

		int cnt = 0;
		while(true){

			rspDataSizeBuffer.put(0, (short)RSP_DATA_SIZE);
			ecode = pm3CsafeCP.tkcmdsetCSAFE_command(unit_address, cmd_data_size, cmdDataBuffer, rspDataSizeBuffer, rspDataBuffer);

			if(ecode != 0){
				printError(ecode);
				short to = 1;
				pm3ddicp.tkcmdsetDDI_shutdown(to);
				fail("execution failed");
			}

			short sz_response = rspDataSizeBuffer.get(0);
			
			stringBuilder =  new StringBuilder();
			stringBuilder.append(String.format("%04d : %s [", cnt++, sz_response));	
			if(sz_response > 0){
				int i=0;
				for(i=0; i < sz_response; i++){
					stringBuilder.append(String.format("%02X%c", rspDataBuffer.get(i), (i < sz_response - 1 ? ':' : ']')));
				}
			
			}
			slf4jLogger.info(stringBuilder.toString());
		
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}		
					
		
	}		
	
	
    private void printError(short ecode){
        
    	byte buffer1[] = new byte[512];
    	ByteBuffer byteBuffer1 = ByteBuffer.wrap(buffer1);

    	// Format the error by looking up info in INI file
    	pm3ddicp.tkcmdsetDDI_get_error_name(ecode, byteBuffer1, (short)512);
    	  	
    	byte buffer2[] = new byte[512];
    	ByteBuffer byteBuffer2 = ByteBuffer.wrap(buffer2);   	
    	pm3ddicp.tkcmdsetDDI_get_error_text(ecode,byteBuffer2, (short)512);
          
    	slf4jLogger.info(String.format("Error message 1 %s\n", new String(byteBuffer1.array())));
    	slf4jLogger.info(String.format("Error message 2 %s\n", new String(byteBuffer2.array())));    	
    	
    }
}
