#ifndef _PM3_H_
#define  _PM3_H_


extern int PM3TRACE;

#define PM_TRACE_INIT(module)														\
	if(getenv ("pm3.trace")){														\
		PM3TRACE = 1;																\
		pm3_trace("%s; module: %s ; build date: %s; build time: %s",				\
						"trace enabled",											\
						module, __DATE__, __TIME__);								\
	}																				\

	
#define pm3_trace(fmt, ...)															\
        do { if (PM3TRACE) printf("%s:%d> " fmt "\n", __FUNCDNAME__, __LINE__,		\
				__VA_ARGS__); } while (0)											


#define PM3_TRACE_START pm3_trace("%s", "start")

#define PM3_TRACE_END(_res) pm3_trace("%s, _res: %d", "end", _res)

#define pm3_hexdump(msg, buffer, len)												\
	do {																			\
		if (PM3TRACE){																\
			printf("%s:%d\n", __FUNCTION__, __LINE__);								\
			printf("%s=[", msg);													\
			for(int i=0; i < len; i++){												\
			printf("%X%c", buffer[i], (i<len-1 ? ':' : ']'));						\
			}																		\
			printf("\n");															\
		}																			\
																					\
	} while (0)


#define pm3_printhex(msg, buffer, len)												\
			pm3_hexdump(msg, buffer, len);											\
			printf("%s\n", buffer);

#endif