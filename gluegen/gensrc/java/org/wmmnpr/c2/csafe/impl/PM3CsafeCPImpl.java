/* !---- DO NOT EDIT: This file autogenerated by com\jogamp\gluegen\JavaEmitter.java on Tue Jul 29 23:08:39 CEST 2014 ----! */

package org.wmmnpr.c2.csafe.impl;

import com.jogamp.gluegen.runtime.*;
import com.jogamp.common.os.*;
import com.jogamp.common.nio.*;
import java.nio.*;

public class PM3CsafeCPImpl implements PM3CsafeCP{
  /** Interface to C language function: <br> <code> BOOLEAN_T pm3csafecmds_get_index(UINT8_T cmd, UINT16_T *  index); </code> 
      @param index a direct or array-backed {@link java.nio.ShortBuffer}   */
  public byte pm3csafecmds_get_index(byte cmd, ShortBuffer index)  {

    final boolean index_is_direct = Buffers.isDirect(index);
        return pm3csafecmds_get_index1(cmd, index_is_direct ? index : Buffers.getArray(index), index_is_direct ? Buffers.getDirectBufferByteOffset(index) : Buffers.getIndirectBufferByteOffset(index), index_is_direct);
  }

  /** Entry point to C language function: <code> BOOLEAN_T pm3csafecmds_get_index(UINT8_T cmd, UINT16_T *  index); </code> 
      @param index a direct or array-backed {@link java.nio.ShortBuffer}   */
  private native byte pm3csafecmds_get_index1(byte cmd, Object index, int index_byte_offset, boolean index_is_direct);

  /** Interface to C language function: <br> <code> BOOLEAN_T pm3csafecmds_get_index(UINT8_T cmd, UINT16_T *  index); </code>    */
  public byte pm3csafecmds_get_index(byte cmd, short[] index, int index_offset)  {

    if(index != null && index.length <= index_offset)
      throw new RuntimeException("array offset argument \"index_offset\" (" + index_offset + ") equals or exceeds array length (" + index.length + ")");
        return pm3csafecmds_get_index1(cmd, index, Buffers.SIZEOF_SHORT * index_offset, false);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_async_command(UINT16_T unit_address, UINT16_T cmd_data_size, UINT32_T *  cmd_data, UINT16_T cmd_tag); </code> 
      @param cmd_data a direct or array-backed {@link java.nio.LongBuffer}   */
  public short tkcmdsetCSAFE_async_command(short unit_address, short cmd_data_size, LongBuffer cmd_data, short cmd_tag)  {

    final boolean cmd_data_is_direct = Buffers.isDirect(cmd_data);
        return tkcmdsetCSAFE_async_command1(unit_address, cmd_data_size, cmd_data_is_direct ? cmd_data : Buffers.getArray(cmd_data), cmd_data_is_direct ? Buffers.getDirectBufferByteOffset(cmd_data) : Buffers.getIndirectBufferByteOffset(cmd_data), cmd_data_is_direct, cmd_tag);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_async_command(UINT16_T unit_address, UINT16_T cmd_data_size, UINT32_T *  cmd_data, UINT16_T cmd_tag); </code> 
      @param cmd_data a direct or array-backed {@link java.nio.LongBuffer}   */
  private native short tkcmdsetCSAFE_async_command1(short unit_address, short cmd_data_size, Object cmd_data, int cmd_data_byte_offset, boolean cmd_data_is_direct, short cmd_tag);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_async_command(UINT16_T unit_address, UINT16_T cmd_data_size, UINT32_T *  cmd_data, UINT16_T cmd_tag); </code>    */
  public short tkcmdsetCSAFE_async_command(short unit_address, short cmd_data_size, long[] cmd_data, int cmd_data_offset, short cmd_tag)  {

    if(cmd_data != null && cmd_data.length <= cmd_data_offset)
      throw new RuntimeException("array offset argument \"cmd_data_offset\" (" + cmd_data_offset + ") equals or exceeds array length (" + cmd_data.length + ")");
        return tkcmdsetCSAFE_async_command1(unit_address, cmd_data_size, cmd_data, Buffers.SIZEOF_LONG * cmd_data_offset, false, cmd_tag);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_command(UINT16_T unit_address, UINT16_T cmd_data_size, UINT32_T *  cmd_data, UINT16_T *  rsp_data_size, UINT32_T *  rsp_data); </code> 
      @param cmd_data a direct or array-backed {@link java.nio.LongBuffer}
      @param rsp_data_size a direct or array-backed {@link java.nio.ShortBuffer}
      @param rsp_data a direct or array-backed {@link java.nio.LongBuffer}   */
  public short tkcmdsetCSAFE_command(short unit_address, short cmd_data_size, LongBuffer cmd_data, ShortBuffer rsp_data_size, LongBuffer rsp_data)  {

    final boolean cmd_data_is_direct = Buffers.isDirect(cmd_data);
    final boolean rsp_data_size_is_direct = Buffers.isDirect(rsp_data_size);
    final boolean rsp_data_is_direct = Buffers.isDirect(rsp_data);
        return tkcmdsetCSAFE_command1(unit_address, cmd_data_size, cmd_data_is_direct ? cmd_data : Buffers.getArray(cmd_data), cmd_data_is_direct ? Buffers.getDirectBufferByteOffset(cmd_data) : Buffers.getIndirectBufferByteOffset(cmd_data), cmd_data_is_direct, rsp_data_size_is_direct ? rsp_data_size : Buffers.getArray(rsp_data_size), rsp_data_size_is_direct ? Buffers.getDirectBufferByteOffset(rsp_data_size) : Buffers.getIndirectBufferByteOffset(rsp_data_size), rsp_data_size_is_direct, rsp_data_is_direct ? rsp_data : Buffers.getArray(rsp_data), rsp_data_is_direct ? Buffers.getDirectBufferByteOffset(rsp_data) : Buffers.getIndirectBufferByteOffset(rsp_data), rsp_data_is_direct);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_command(UINT16_T unit_address, UINT16_T cmd_data_size, UINT32_T *  cmd_data, UINT16_T *  rsp_data_size, UINT32_T *  rsp_data); </code> 
      @param cmd_data a direct or array-backed {@link java.nio.LongBuffer}
      @param rsp_data_size a direct or array-backed {@link java.nio.ShortBuffer}
      @param rsp_data a direct or array-backed {@link java.nio.LongBuffer}   */
  private native short tkcmdsetCSAFE_command1(short unit_address, short cmd_data_size, Object cmd_data, int cmd_data_byte_offset, boolean cmd_data_is_direct, Object rsp_data_size, int rsp_data_size_byte_offset, boolean rsp_data_size_is_direct, Object rsp_data, int rsp_data_byte_offset, boolean rsp_data_is_direct);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_command(UINT16_T unit_address, UINT16_T cmd_data_size, UINT32_T *  cmd_data, UINT16_T *  rsp_data_size, UINT32_T *  rsp_data); </code>    */
  public short tkcmdsetCSAFE_command(short unit_address, short cmd_data_size, long[] cmd_data, int cmd_data_offset, short[] rsp_data_size, int rsp_data_size_offset, long[] rsp_data, int rsp_data_offset)  {

    if(cmd_data != null && cmd_data.length <= cmd_data_offset)
      throw new RuntimeException("array offset argument \"cmd_data_offset\" (" + cmd_data_offset + ") equals or exceeds array length (" + cmd_data.length + ")");
    if(rsp_data_size != null && rsp_data_size.length <= rsp_data_size_offset)
      throw new RuntimeException("array offset argument \"rsp_data_size_offset\" (" + rsp_data_size_offset + ") equals or exceeds array length (" + rsp_data_size.length + ")");
    if(rsp_data != null && rsp_data.length <= rsp_data_offset)
      throw new RuntimeException("array offset argument \"rsp_data_offset\" (" + rsp_data_offset + ") equals or exceeds array length (" + rsp_data.length + ")");
        return tkcmdsetCSAFE_command1(unit_address, cmd_data_size, cmd_data, Buffers.SIZEOF_LONG * cmd_data_offset, false, rsp_data_size, Buffers.SIZEOF_SHORT * rsp_data_size_offset, false, rsp_data, Buffers.SIZEOF_LONG * rsp_data_offset, false);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_data_types(UINT8_T cmd, UINT8_T *  cmd_type, UINT8_T *  num_cmd_data_types, UINT8_T *  cmd_data_type, UINT8_T *  num_rsp_data_types, UINT8_T *  rsp_data_type); </code> 
      @param cmd_type a direct or array-backed {@link java.nio.ByteBuffer}
      @param num_cmd_data_types a direct or array-backed {@link java.nio.ByteBuffer}
      @param cmd_data_type a direct or array-backed {@link java.nio.ByteBuffer}
      @param num_rsp_data_types a direct or array-backed {@link java.nio.ByteBuffer}
      @param rsp_data_type a direct or array-backed {@link java.nio.ByteBuffer}   */
  public short tkcmdsetCSAFE_get_cmd_data_types(byte cmd, ByteBuffer cmd_type, ByteBuffer num_cmd_data_types, ByteBuffer cmd_data_type, ByteBuffer num_rsp_data_types, ByteBuffer rsp_data_type)  {

    final boolean cmd_type_is_direct = Buffers.isDirect(cmd_type);
    final boolean num_cmd_data_types_is_direct = Buffers.isDirect(num_cmd_data_types);
    final boolean cmd_data_type_is_direct = Buffers.isDirect(cmd_data_type);
    final boolean num_rsp_data_types_is_direct = Buffers.isDirect(num_rsp_data_types);
    final boolean rsp_data_type_is_direct = Buffers.isDirect(rsp_data_type);
        return tkcmdsetCSAFE_get_cmd_data_types1(cmd, cmd_type_is_direct ? cmd_type : Buffers.getArray(cmd_type), cmd_type_is_direct ? Buffers.getDirectBufferByteOffset(cmd_type) : Buffers.getIndirectBufferByteOffset(cmd_type), cmd_type_is_direct, num_cmd_data_types_is_direct ? num_cmd_data_types : Buffers.getArray(num_cmd_data_types), num_cmd_data_types_is_direct ? Buffers.getDirectBufferByteOffset(num_cmd_data_types) : Buffers.getIndirectBufferByteOffset(num_cmd_data_types), num_cmd_data_types_is_direct, cmd_data_type_is_direct ? cmd_data_type : Buffers.getArray(cmd_data_type), cmd_data_type_is_direct ? Buffers.getDirectBufferByteOffset(cmd_data_type) : Buffers.getIndirectBufferByteOffset(cmd_data_type), cmd_data_type_is_direct, num_rsp_data_types_is_direct ? num_rsp_data_types : Buffers.getArray(num_rsp_data_types), num_rsp_data_types_is_direct ? Buffers.getDirectBufferByteOffset(num_rsp_data_types) : Buffers.getIndirectBufferByteOffset(num_rsp_data_types), num_rsp_data_types_is_direct, rsp_data_type_is_direct ? rsp_data_type : Buffers.getArray(rsp_data_type), rsp_data_type_is_direct ? Buffers.getDirectBufferByteOffset(rsp_data_type) : Buffers.getIndirectBufferByteOffset(rsp_data_type), rsp_data_type_is_direct);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_data_types(UINT8_T cmd, UINT8_T *  cmd_type, UINT8_T *  num_cmd_data_types, UINT8_T *  cmd_data_type, UINT8_T *  num_rsp_data_types, UINT8_T *  rsp_data_type); </code> 
      @param cmd_type a direct or array-backed {@link java.nio.ByteBuffer}
      @param num_cmd_data_types a direct or array-backed {@link java.nio.ByteBuffer}
      @param cmd_data_type a direct or array-backed {@link java.nio.ByteBuffer}
      @param num_rsp_data_types a direct or array-backed {@link java.nio.ByteBuffer}
      @param rsp_data_type a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native short tkcmdsetCSAFE_get_cmd_data_types1(byte cmd, Object cmd_type, int cmd_type_byte_offset, boolean cmd_type_is_direct, Object num_cmd_data_types, int num_cmd_data_types_byte_offset, boolean num_cmd_data_types_is_direct, Object cmd_data_type, int cmd_data_type_byte_offset, boolean cmd_data_type_is_direct, Object num_rsp_data_types, int num_rsp_data_types_byte_offset, boolean num_rsp_data_types_is_direct, Object rsp_data_type, int rsp_data_type_byte_offset, boolean rsp_data_type_is_direct);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_data_types(UINT8_T cmd, UINT8_T *  cmd_type, UINT8_T *  num_cmd_data_types, UINT8_T *  cmd_data_type, UINT8_T *  num_rsp_data_types, UINT8_T *  rsp_data_type); </code>    */
  public short tkcmdsetCSAFE_get_cmd_data_types(byte cmd, byte[] cmd_type, int cmd_type_offset, byte[] num_cmd_data_types, int num_cmd_data_types_offset, byte[] cmd_data_type, int cmd_data_type_offset, byte[] num_rsp_data_types, int num_rsp_data_types_offset, byte[] rsp_data_type, int rsp_data_type_offset)  {

    if(cmd_type != null && cmd_type.length <= cmd_type_offset)
      throw new RuntimeException("array offset argument \"cmd_type_offset\" (" + cmd_type_offset + ") equals or exceeds array length (" + cmd_type.length + ")");
    if(num_cmd_data_types != null && num_cmd_data_types.length <= num_cmd_data_types_offset)
      throw new RuntimeException("array offset argument \"num_cmd_data_types_offset\" (" + num_cmd_data_types_offset + ") equals or exceeds array length (" + num_cmd_data_types.length + ")");
    if(cmd_data_type != null && cmd_data_type.length <= cmd_data_type_offset)
      throw new RuntimeException("array offset argument \"cmd_data_type_offset\" (" + cmd_data_type_offset + ") equals or exceeds array length (" + cmd_data_type.length + ")");
    if(num_rsp_data_types != null && num_rsp_data_types.length <= num_rsp_data_types_offset)
      throw new RuntimeException("array offset argument \"num_rsp_data_types_offset\" (" + num_rsp_data_types_offset + ") equals or exceeds array length (" + num_rsp_data_types.length + ")");
    if(rsp_data_type != null && rsp_data_type.length <= rsp_data_type_offset)
      throw new RuntimeException("array offset argument \"rsp_data_type_offset\" (" + rsp_data_type_offset + ") equals or exceeds array length (" + rsp_data_type.length + ")");
        return tkcmdsetCSAFE_get_cmd_data_types1(cmd, cmd_type, cmd_type_offset, false, num_cmd_data_types, num_cmd_data_types_offset, false, cmd_data_type, cmd_data_type_offset, false, num_rsp_data_types, num_rsp_data_types_offset, false, rsp_data_type, rsp_data_type_offset, false);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_name(UINT8_T cmd, char *  textptr, UINT16_T textlen); </code> 
      @param textptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  public short tkcmdsetCSAFE_get_cmd_name(byte cmd, ByteBuffer textptr, short textlen)  {

    final boolean textptr_is_direct = Buffers.isDirect(textptr);
        return tkcmdsetCSAFE_get_cmd_name1(cmd, textptr_is_direct ? textptr : Buffers.getArray(textptr), textptr_is_direct ? Buffers.getDirectBufferByteOffset(textptr) : Buffers.getIndirectBufferByteOffset(textptr), textptr_is_direct, textlen);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_name(UINT8_T cmd, char *  textptr, UINT16_T textlen); </code> 
      @param textptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native short tkcmdsetCSAFE_get_cmd_name1(byte cmd, Object textptr, int textptr_byte_offset, boolean textptr_is_direct, short textlen);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_name(UINT8_T cmd, char *  textptr, UINT16_T textlen); </code>    */
  public short tkcmdsetCSAFE_get_cmd_name(byte cmd, byte[] textptr, int textptr_offset, short textlen)  {

    if(textptr != null && textptr.length <= textptr_offset)
      throw new RuntimeException("array offset argument \"textptr_offset\" (" + textptr_offset + ") equals or exceeds array length (" + textptr.length + ")");
        return tkcmdsetCSAFE_get_cmd_name1(cmd, textptr, textptr_offset, false, textlen);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_text(UINT8_T cmd, char *  textptr, UINT16_T textlen); </code> 
      @param textptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  public short tkcmdsetCSAFE_get_cmd_text(byte cmd, ByteBuffer textptr, short textlen)  {

    final boolean textptr_is_direct = Buffers.isDirect(textptr);
        return tkcmdsetCSAFE_get_cmd_text1(cmd, textptr_is_direct ? textptr : Buffers.getArray(textptr), textptr_is_direct ? Buffers.getDirectBufferByteOffset(textptr) : Buffers.getIndirectBufferByteOffset(textptr), textptr_is_direct, textlen);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_text(UINT8_T cmd, char *  textptr, UINT16_T textlen); </code> 
      @param textptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native short tkcmdsetCSAFE_get_cmd_text1(byte cmd, Object textptr, int textptr_byte_offset, boolean textptr_is_direct, short textlen);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_cmd_text(UINT8_T cmd, char *  textptr, UINT16_T textlen); </code>    */
  public short tkcmdsetCSAFE_get_cmd_text(byte cmd, byte[] textptr, int textptr_offset, short textlen)  {

    if(textptr != null && textptr.length <= textptr_offset)
      throw new RuntimeException("array offset argument \"textptr_offset\" (" + textptr_offset + ") equals or exceeds array length (" + textptr.length + ")");
        return tkcmdsetCSAFE_get_cmd_text1(cmd, textptr, textptr_offset, false, textlen);
  }

  /** Interface to C language function: <br> <code> UINT16_T tkcmdsetCSAFE_get_dll_version(void); </code>    */
  public native short tkcmdsetCSAFE_get_dll_version();

  /** Interface to C language function: <br> <code> void tkcmdsetCSAFE_get_error_name(ERRCODE_T ecode, char *  nameptr, UINT16_T namelen); </code> 
      @param nameptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  public void tkcmdsetCSAFE_get_error_name(short ecode, ByteBuffer nameptr, short namelen)  {

    final boolean nameptr_is_direct = Buffers.isDirect(nameptr);
        tkcmdsetCSAFE_get_error_name1(ecode, nameptr_is_direct ? nameptr : Buffers.getArray(nameptr), nameptr_is_direct ? Buffers.getDirectBufferByteOffset(nameptr) : Buffers.getIndirectBufferByteOffset(nameptr), nameptr_is_direct, namelen);
  }

  /** Entry point to C language function: <code> void tkcmdsetCSAFE_get_error_name(ERRCODE_T ecode, char *  nameptr, UINT16_T namelen); </code> 
      @param nameptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native void tkcmdsetCSAFE_get_error_name1(short ecode, Object nameptr, int nameptr_byte_offset, boolean nameptr_is_direct, short namelen);

  /** Interface to C language function: <br> <code> void tkcmdsetCSAFE_get_error_name(ERRCODE_T ecode, char *  nameptr, UINT16_T namelen); </code>    */
  public void tkcmdsetCSAFE_get_error_name(short ecode, byte[] nameptr, int nameptr_offset, short namelen)  {

    if(nameptr != null && nameptr.length <= nameptr_offset)
      throw new RuntimeException("array offset argument \"nameptr_offset\" (" + nameptr_offset + ") equals or exceeds array length (" + nameptr.length + ")");
        tkcmdsetCSAFE_get_error_name1(ecode, nameptr, nameptr_offset, false, namelen);
  }

  /** Interface to C language function: <br> <code> void tkcmdsetCSAFE_get_error_text(ERRCODE_T ecode, char *  textptr, UINT16_T textlen); </code> 
      @param textptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  public void tkcmdsetCSAFE_get_error_text(short ecode, ByteBuffer textptr, short textlen)  {

    final boolean textptr_is_direct = Buffers.isDirect(textptr);
        tkcmdsetCSAFE_get_error_text1(ecode, textptr_is_direct ? textptr : Buffers.getArray(textptr), textptr_is_direct ? Buffers.getDirectBufferByteOffset(textptr) : Buffers.getIndirectBufferByteOffset(textptr), textptr_is_direct, textlen);
  }

  /** Entry point to C language function: <code> void tkcmdsetCSAFE_get_error_text(ERRCODE_T ecode, char *  textptr, UINT16_T textlen); </code> 
      @param textptr a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native void tkcmdsetCSAFE_get_error_text1(short ecode, Object textptr, int textptr_byte_offset, boolean textptr_is_direct, short textlen);

  /** Interface to C language function: <br> <code> void tkcmdsetCSAFE_get_error_text(ERRCODE_T ecode, char *  textptr, UINT16_T textlen); </code>    */
  public void tkcmdsetCSAFE_get_error_text(short ecode, byte[] textptr, int textptr_offset, short textlen)  {

    if(textptr != null && textptr.length <= textptr_offset)
      throw new RuntimeException("array offset argument \"textptr_offset\" (" + textptr_offset + ") equals or exceeds array length (" + textptr.length + ")");
        tkcmdsetCSAFE_get_error_text1(ecode, textptr, textptr_offset, false, textlen);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_logcard_state(UINT16_T unit_address, UINT8_T *  state); </code> 
      @param state a direct or array-backed {@link java.nio.ByteBuffer}   */
  public short tkcmdsetCSAFE_get_logcard_state(short unit_address, ByteBuffer state)  {

    final boolean state_is_direct = Buffers.isDirect(state);
        return tkcmdsetCSAFE_get_logcard_state1(unit_address, state_is_direct ? state : Buffers.getArray(state), state_is_direct ? Buffers.getDirectBufferByteOffset(state) : Buffers.getIndirectBufferByteOffset(state), state_is_direct);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_get_logcard_state(UINT16_T unit_address, UINT8_T *  state); </code> 
      @param state a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native short tkcmdsetCSAFE_get_logcard_state1(short unit_address, Object state, int state_byte_offset, boolean state_is_direct);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_logcard_state(UINT16_T unit_address, UINT8_T *  state); </code>    */
  public short tkcmdsetCSAFE_get_logcard_state(short unit_address, byte[] state, int state_offset)  {

    if(state != null && state.length <= state_offset)
      throw new RuntimeException("array offset argument \"state_offset\" (" + state_offset + ") equals or exceeds array length (" + state.length + ")");
        return tkcmdsetCSAFE_get_logcard_state1(unit_address, state, state_offset, false);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_logcard_status(UINT16_T unit_address, UINT8_T *  status); </code> 
      @param status a direct or array-backed {@link java.nio.ByteBuffer}   */
  public short tkcmdsetCSAFE_get_logcard_status(short unit_address, ByteBuffer status)  {

    final boolean status_is_direct = Buffers.isDirect(status);
        return tkcmdsetCSAFE_get_logcard_status1(unit_address, status_is_direct ? status : Buffers.getArray(status), status_is_direct ? Buffers.getDirectBufferByteOffset(status) : Buffers.getIndirectBufferByteOffset(status), status_is_direct);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_get_logcard_status(UINT16_T unit_address, UINT8_T *  status); </code> 
      @param status a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native short tkcmdsetCSAFE_get_logcard_status1(short unit_address, Object status, int status_byte_offset, boolean status_is_direct);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_get_logcard_status(UINT16_T unit_address, UINT8_T *  status); </code>    */
  public short tkcmdsetCSAFE_get_logcard_status(short unit_address, byte[] status, int status_offset)  {

    if(status != null && status.length <= status_offset)
      throw new RuntimeException("array offset argument \"status_offset\" (" + status_offset + ") equals or exceeds array length (" + status.length + ")");
        return tkcmdsetCSAFE_get_logcard_status1(unit_address, status, status_offset, false);
  }

  /** Interface to C language function: <br> <code> UINT8_T tkcmdsetCSAFE_get_status(void); </code>    */
  public native byte tkcmdsetCSAFE_get_status();

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_init_protocol(UINT16_T timeout); </code>    */
  public native short tkcmdsetCSAFE_init_protocol(short timeout);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_read_logblk8(UINT16_T unit_address, UINT32_T logcard_address, UINT32_T byte_len, UINT8_T *  val_ptr8, UINT32_T *  num_read); </code> 
      @param val_ptr8 a direct or array-backed {@link java.nio.ByteBuffer}
      @param num_read a direct or array-backed {@link java.nio.LongBuffer}   */
  public short tkcmdsetCSAFE_read_logblk8(short unit_address, long logcard_address, long byte_len, ByteBuffer val_ptr8, LongBuffer num_read)  {

    final boolean val_ptr8_is_direct = Buffers.isDirect(val_ptr8);
    final boolean num_read_is_direct = Buffers.isDirect(num_read);
        return tkcmdsetCSAFE_read_logblk81(unit_address, logcard_address, byte_len, val_ptr8_is_direct ? val_ptr8 : Buffers.getArray(val_ptr8), val_ptr8_is_direct ? Buffers.getDirectBufferByteOffset(val_ptr8) : Buffers.getIndirectBufferByteOffset(val_ptr8), val_ptr8_is_direct, num_read_is_direct ? num_read : Buffers.getArray(num_read), num_read_is_direct ? Buffers.getDirectBufferByteOffset(num_read) : Buffers.getIndirectBufferByteOffset(num_read), num_read_is_direct);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_read_logblk8(UINT16_T unit_address, UINT32_T logcard_address, UINT32_T byte_len, UINT8_T *  val_ptr8, UINT32_T *  num_read); </code> 
      @param val_ptr8 a direct or array-backed {@link java.nio.ByteBuffer}
      @param num_read a direct or array-backed {@link java.nio.LongBuffer}   */
  private native short tkcmdsetCSAFE_read_logblk81(short unit_address, long logcard_address, long byte_len, Object val_ptr8, int val_ptr8_byte_offset, boolean val_ptr8_is_direct, Object num_read, int num_read_byte_offset, boolean num_read_is_direct);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_read_logblk8(UINT16_T unit_address, UINT32_T logcard_address, UINT32_T byte_len, UINT8_T *  val_ptr8, UINT32_T *  num_read); </code>    */
  public short tkcmdsetCSAFE_read_logblk8(short unit_address, long logcard_address, long byte_len, byte[] val_ptr8, int val_ptr8_offset, long[] num_read, int num_read_offset)  {

    if(val_ptr8 != null && val_ptr8.length <= val_ptr8_offset)
      throw new RuntimeException("array offset argument \"val_ptr8_offset\" (" + val_ptr8_offset + ") equals or exceeds array length (" + val_ptr8.length + ")");
    if(num_read != null && num_read.length <= num_read_offset)
      throw new RuntimeException("array offset argument \"num_read_offset\" (" + num_read_offset + ") equals or exceeds array length (" + num_read.length + ")");
        return tkcmdsetCSAFE_read_logblk81(unit_address, logcard_address, byte_len, val_ptr8, val_ptr8_offset, false, num_read, Buffers.SIZEOF_LONG * num_read_offset, false);
  }

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_write_logblk8(UINT16_T unit_address, UINT32_T logcard_address, UINT8_T *  val_ptr8, UINT32_T byte_len); </code> 
      @param val_ptr8 a direct or array-backed {@link java.nio.ByteBuffer}   */
  public short tkcmdsetCSAFE_write_logblk8(short unit_address, long logcard_address, ByteBuffer val_ptr8, long byte_len)  {

    final boolean val_ptr8_is_direct = Buffers.isDirect(val_ptr8);
        return tkcmdsetCSAFE_write_logblk81(unit_address, logcard_address, val_ptr8_is_direct ? val_ptr8 : Buffers.getArray(val_ptr8), val_ptr8_is_direct ? Buffers.getDirectBufferByteOffset(val_ptr8) : Buffers.getIndirectBufferByteOffset(val_ptr8), val_ptr8_is_direct, byte_len);
  }

  /** Entry point to C language function: <code> ERRCODE_T tkcmdsetCSAFE_write_logblk8(UINT16_T unit_address, UINT32_T logcard_address, UINT8_T *  val_ptr8, UINT32_T byte_len); </code> 
      @param val_ptr8 a direct or array-backed {@link java.nio.ByteBuffer}   */
  private native short tkcmdsetCSAFE_write_logblk81(short unit_address, long logcard_address, Object val_ptr8, int val_ptr8_byte_offset, boolean val_ptr8_is_direct, long byte_len);

  /** Interface to C language function: <br> <code> ERRCODE_T tkcmdsetCSAFE_write_logblk8(UINT16_T unit_address, UINT32_T logcard_address, UINT8_T *  val_ptr8, UINT32_T byte_len); </code>    */
  public short tkcmdsetCSAFE_write_logblk8(short unit_address, long logcard_address, byte[] val_ptr8, int val_ptr8_offset, long byte_len)  {

    if(val_ptr8 != null && val_ptr8.length <= val_ptr8_offset)
      throw new RuntimeException("array offset argument \"val_ptr8_offset\" (" + val_ptr8_offset + ") equals or exceeds array length (" + val_ptr8.length + ")");
        return tkcmdsetCSAFE_write_logblk81(unit_address, logcard_address, val_ptr8, val_ptr8_offset, false, byte_len);
  }


} // end of class PM3CsafeCPImpl
