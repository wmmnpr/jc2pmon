/* !---- DO NOT EDIT: This file autogenerated by com\jogamp\gluegen\JavaEmitter.java on Tue Jul 29 23:08:39 CEST 2014 ----! */

#include <jni.h>
#include <stdlib.h>

#include <assert.h>

 #include <PM3USBCP.h>

/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_do_DDIcommand(short port, java.nio.ByteBuffer cmd_ptr, short cmd_len, java.nio.ByteBuffer rsp_ptr, java.nio.ShortBuffer rsp_len, short timeout)
 *     C function: ERRCODE_T tkcmdsetUSB_do_DDIcommand(UINT16_T port, UINT8_T *  cmd_ptr, UINT16_T cmd_len, UINT8_T *  rsp_ptr, UINT16_T *  rsp_len, UINT16_T timeout);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1do_1DDIcommand1__SLjava_lang_Object_2IZSLjava_lang_Object_2IZLjava_lang_Object_2IZS(JNIEnv *env, jobject _unused, jshort port, jobject cmd_ptr, jint cmd_ptr_byte_offset, jboolean cmd_ptr_is_nio, jshort cmd_len, jobject rsp_ptr, jint rsp_ptr_byte_offset, jboolean rsp_ptr_is_nio, jobject rsp_len, jint rsp_len_byte_offset, jboolean rsp_len_is_nio, jshort timeout) {
  UINT8_T * _cmd_ptr_ptr = NULL;
  UINT8_T * _rsp_ptr_ptr = NULL;
  UINT16_T * _rsp_len_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != cmd_ptr ) {
    _cmd_ptr_ptr = (UINT8_T *) ( JNI_TRUE == cmd_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, cmd_ptr) :  (*env)->GetPrimitiveArrayCritical(env, cmd_ptr, NULL) );  }
  if ( NULL != rsp_ptr ) {
    _rsp_ptr_ptr = (UINT8_T *) ( JNI_TRUE == rsp_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, rsp_ptr) :  (*env)->GetPrimitiveArrayCritical(env, rsp_ptr, NULL) );  }
  if ( NULL != rsp_len ) {
    _rsp_len_ptr = (UINT16_T *) ( JNI_TRUE == rsp_len_is_nio ?  (*env)->GetDirectBufferAddress(env, rsp_len) :  (*env)->GetPrimitiveArrayCritical(env, rsp_len, NULL) );  }
  _res = tkcmdsetUSB_do_DDIcommand((UINT16_T) port, (UINT8_T *) (((char *) _cmd_ptr_ptr) + cmd_ptr_byte_offset), (UINT16_T) cmd_len, (UINT8_T *) (((char *) _rsp_ptr_ptr) + rsp_ptr_byte_offset), (UINT16_T *) (((char *) _rsp_len_ptr) + rsp_len_byte_offset), (UINT16_T) timeout);
  if ( JNI_FALSE == cmd_ptr_is_nio && NULL != cmd_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, cmd_ptr, _cmd_ptr_ptr, 0);  }
  if ( JNI_FALSE == rsp_ptr_is_nio && NULL != rsp_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, rsp_ptr, _rsp_ptr_ptr, 0);  }
  if ( JNI_FALSE == rsp_len_is_nio && NULL != rsp_len ) {
    (*env)->ReleasePrimitiveArrayCritical(env, rsp_len, _rsp_len_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_do_command(short port, java.nio.ByteBuffer tx_ptr, short tx_len, java.nio.ByteBuffer rx_ptr, java.nio.ShortBuffer rx_len, short timeout)
 *     C function: ERRCODE_T tkcmdsetUSB_do_command(UINT16_T port, UINT8_T *  tx_ptr, UINT16_T tx_len, UINT8_T *  rx_ptr, UINT16_T *  rx_len, UINT16_T timeout);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1do_1command1__SLjava_lang_Object_2IZSLjava_lang_Object_2IZLjava_lang_Object_2IZS(JNIEnv *env, jobject _unused, jshort port, jobject tx_ptr, jint tx_ptr_byte_offset, jboolean tx_ptr_is_nio, jshort tx_len, jobject rx_ptr, jint rx_ptr_byte_offset, jboolean rx_ptr_is_nio, jobject rx_len, jint rx_len_byte_offset, jboolean rx_len_is_nio, jshort timeout) {
  UINT8_T * _tx_ptr_ptr = NULL;
  UINT8_T * _rx_ptr_ptr = NULL;
  UINT16_T * _rx_len_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != tx_ptr ) {
    _tx_ptr_ptr = (UINT8_T *) ( JNI_TRUE == tx_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, tx_ptr) :  (*env)->GetPrimitiveArrayCritical(env, tx_ptr, NULL) );  }
  if ( NULL != rx_ptr ) {
    _rx_ptr_ptr = (UINT8_T *) ( JNI_TRUE == rx_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, rx_ptr) :  (*env)->GetPrimitiveArrayCritical(env, rx_ptr, NULL) );  }
  if ( NULL != rx_len ) {
    _rx_len_ptr = (UINT16_T *) ( JNI_TRUE == rx_len_is_nio ?  (*env)->GetDirectBufferAddress(env, rx_len) :  (*env)->GetPrimitiveArrayCritical(env, rx_len, NULL) );  }
  _res = tkcmdsetUSB_do_command((UINT16_T) port, (UINT8_T *) (((char *) _tx_ptr_ptr) + tx_ptr_byte_offset), (UINT16_T) tx_len, (UINT8_T *) (((char *) _rx_ptr_ptr) + rx_ptr_byte_offset), (UINT16_T *) (((char *) _rx_len_ptr) + rx_len_byte_offset), (UINT16_T) timeout);
  if ( JNI_FALSE == tx_ptr_is_nio && NULL != tx_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, tx_ptr, _tx_ptr_ptr, 0);  }
  if ( JNI_FALSE == rx_ptr_is_nio && NULL != rx_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, rx_ptr, _rx_ptr_ptr, 0);  }
  if ( JNI_FALSE == rx_len_is_nio && NULL != rx_len ) {
    (*env)->ReleasePrimitiveArrayCritical(env, rx_len, _rx_len_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_echo(short port, java.nio.ByteBuffer cmd_ptr, short cmd_len)
 *     C function: ERRCODE_T tkcmdsetUSB_echo(UINT16_T port, UINT8_T *  cmd_ptr, UINT16_T cmd_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1echo1__SLjava_lang_Object_2IZS(JNIEnv *env, jobject _unused, jshort port, jobject cmd_ptr, jint cmd_ptr_byte_offset, jboolean cmd_ptr_is_nio, jshort cmd_len) {
  UINT8_T * _cmd_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != cmd_ptr ) {
    _cmd_ptr_ptr = (UINT8_T *) ( JNI_TRUE == cmd_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, cmd_ptr) :  (*env)->GetPrimitiveArrayCritical(env, cmd_ptr, NULL) );  }
  _res = tkcmdsetUSB_echo((UINT16_T) port, (UINT8_T *) (((char *) _cmd_ptr_ptr) + cmd_ptr_byte_offset), (UINT16_T) cmd_len);
  if ( JNI_FALSE == cmd_ptr_is_nio && NULL != cmd_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, cmd_ptr, _cmd_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_find_devices(java.nio.ByteBuffer product_name, java.nio.ByteBuffer num_found, java.nio.ShortBuffer port_list)
 *     C function: ERRCODE_T tkcmdsetUSB_find_devices(INT8_T *  product_name, UINT8_T *  num_found, UINT16_T *  port_list);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1find_1devices1__Ljava_lang_Object_2IZLjava_lang_Object_2IZLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jobject product_name, jint product_name_byte_offset, jboolean product_name_is_nio, jobject num_found, jint num_found_byte_offset, jboolean num_found_is_nio, jobject port_list, jint port_list_byte_offset, jboolean port_list_is_nio) {
  INT8_T * _product_name_ptr = NULL;
  UINT8_T * _num_found_ptr = NULL;
  UINT16_T * _port_list_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != product_name ) {
    _product_name_ptr = (INT8_T *) ( JNI_TRUE == product_name_is_nio ?  (*env)->GetDirectBufferAddress(env, product_name) :  (*env)->GetPrimitiveArrayCritical(env, product_name, NULL) );  }
  if ( NULL != num_found ) {
    _num_found_ptr = (UINT8_T *) ( JNI_TRUE == num_found_is_nio ?  (*env)->GetDirectBufferAddress(env, num_found) :  (*env)->GetPrimitiveArrayCritical(env, num_found, NULL) );  }
  if ( NULL != port_list ) {
    _port_list_ptr = (UINT16_T *) ( JNI_TRUE == port_list_is_nio ?  (*env)->GetDirectBufferAddress(env, port_list) :  (*env)->GetPrimitiveArrayCritical(env, port_list, NULL) );  }
  _res = tkcmdsetUSB_find_devices((INT8_T *) (((char *) _product_name_ptr) + product_name_byte_offset), (UINT8_T *) (((char *) _num_found_ptr) + num_found_byte_offset), (UINT16_T *) (((char *) _port_list_ptr) + port_list_byte_offset));
  if ( JNI_FALSE == product_name_is_nio && NULL != product_name ) {
    (*env)->ReleasePrimitiveArrayCritical(env, product_name, _product_name_ptr, 0);  }
  if ( JNI_FALSE == num_found_is_nio && NULL != num_found ) {
    (*env)->ReleasePrimitiveArrayCritical(env, num_found, _num_found_ptr, 0);  }
  if ( JNI_FALSE == port_list_is_nio && NULL != port_list ) {
    (*env)->ReleasePrimitiveArrayCritical(env, port_list, _port_list_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_fw_version(short port, java.nio.ByteBuffer ver_ptr, byte ver_len)
 *     C function: ERRCODE_T tkcmdsetUSB_fw_version(UINT16_T port, INT8_T *  ver_ptr, UINT8_T ver_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1fw_1version1__SLjava_lang_Object_2IZB(JNIEnv *env, jobject _unused, jshort port, jobject ver_ptr, jint ver_ptr_byte_offset, jboolean ver_ptr_is_nio, jbyte ver_len) {
  INT8_T * _ver_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != ver_ptr ) {
    _ver_ptr_ptr = (INT8_T *) ( JNI_TRUE == ver_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, ver_ptr) :  (*env)->GetPrimitiveArrayCritical(env, ver_ptr, NULL) );  }
  _res = tkcmdsetUSB_fw_version((UINT16_T) port, (INT8_T *) (((char *) _ver_ptr_ptr) + ver_ptr_byte_offset), (UINT8_T) ver_len);
  if ( JNI_FALSE == ver_ptr_is_nio && NULL != ver_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, ver_ptr, _ver_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_get_dll_version()
 *     C function: UINT16_T tkcmdsetUSB_get_dll_version(void);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1get_1dll_1version__(JNIEnv *env, jobject _unused) {
  UINT16_T _res;
  _res = tkcmdsetUSB_get_dll_version();
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: void tkcmdsetUSB_get_error_name(short ecode, java.nio.ByteBuffer nameptr, short namelen)
 *     C function: void tkcmdsetUSB_get_error_name(ERRCODE_T ecode, char *  nameptr, UINT16_T namelen);
 */
JNIEXPORT void JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1get_1error_1name1__SLjava_lang_Object_2IZS(JNIEnv *env, jobject _unused, jshort ecode, jobject nameptr, jint nameptr_byte_offset, jboolean nameptr_is_nio, jshort namelen) {
  char * _nameptr_ptr = NULL;
  if ( NULL != nameptr ) {
    _nameptr_ptr = (char *) ( JNI_TRUE == nameptr_is_nio ?  (*env)->GetDirectBufferAddress(env, nameptr) :  (*env)->GetPrimitiveArrayCritical(env, nameptr, NULL) );  }
  tkcmdsetUSB_get_error_name((ERRCODE_T) ecode, (char *) (((char *) _nameptr_ptr) + nameptr_byte_offset), (UINT16_T) namelen);
  if ( JNI_FALSE == nameptr_is_nio && NULL != nameptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, nameptr, _nameptr_ptr, 0);  }
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: void tkcmdsetUSB_get_error_text(short ecode, java.nio.ByteBuffer textptr, short textlen)
 *     C function: void tkcmdsetUSB_get_error_text(ERRCODE_T ecode, char *  textptr, UINT16_T textlen);
 */
JNIEXPORT void JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1get_1error_1text1__SLjava_lang_Object_2IZS(JNIEnv *env, jobject _unused, jshort ecode, jobject textptr, jint textptr_byte_offset, jboolean textptr_is_nio, jshort textlen) {
  char * _textptr_ptr = NULL;
  if ( NULL != textptr ) {
    _textptr_ptr = (char *) ( JNI_TRUE == textptr_is_nio ?  (*env)->GetDirectBufferAddress(env, textptr) :  (*env)->GetPrimitiveArrayCritical(env, textptr, NULL) );  }
  tkcmdsetUSB_get_error_text((ERRCODE_T) ecode, (char *) (((char *) _textptr_ptr) + textptr_byte_offset), (UINT16_T) textlen);
  if ( JNI_FALSE == textptr_is_nio && NULL != textptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, textptr, _textptr_ptr, 0);  }
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_hw_version(short port, java.nio.ByteBuffer ver_ptr, byte ver_len)
 *     C function: ERRCODE_T tkcmdsetUSB_hw_version(UINT16_T port, INT8_T *  ver_ptr, UINT8_T ver_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1hw_1version1__SLjava_lang_Object_2IZB(JNIEnv *env, jobject _unused, jshort port, jobject ver_ptr, jint ver_ptr_byte_offset, jboolean ver_ptr_is_nio, jbyte ver_len) {
  INT8_T * _ver_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != ver_ptr ) {
    _ver_ptr_ptr = (INT8_T *) ( JNI_TRUE == ver_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, ver_ptr) :  (*env)->GetPrimitiveArrayCritical(env, ver_ptr, NULL) );  }
  _res = tkcmdsetUSB_hw_version((UINT16_T) port, (INT8_T *) (((char *) _ver_ptr_ptr) + ver_ptr_byte_offset), (UINT8_T) ver_len);
  if ( JNI_FALSE == ver_ptr_is_nio && NULL != ver_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, ver_ptr, _ver_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_init()
 *     C function: ERRCODE_T tkcmdsetUSB_init();
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1init__(JNIEnv *env, jobject _unused) {
  ERRCODE_T _res;
  _res = tkcmdsetUSB_init();
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_loader_fw_version(short port, java.nio.ByteBuffer ver_ptr, byte ver_len)
 *     C function: ERRCODE_T tkcmdsetUSB_loader_fw_version(UINT16_T port, INT8_T *  ver_ptr, UINT8_T ver_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1loader_1fw_1version1__SLjava_lang_Object_2IZB(JNIEnv *env, jobject _unused, jshort port, jobject ver_ptr, jint ver_ptr_byte_offset, jboolean ver_ptr_is_nio, jbyte ver_len) {
  INT8_T * _ver_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != ver_ptr ) {
    _ver_ptr_ptr = (INT8_T *) ( JNI_TRUE == ver_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, ver_ptr) :  (*env)->GetPrimitiveArrayCritical(env, ver_ptr, NULL) );  }
  _res = tkcmdsetUSB_loader_fw_version((UINT16_T) port, (INT8_T *) (((char *) _ver_ptr_ptr) + ver_ptr_byte_offset), (UINT8_T) ver_len);
  if ( JNI_FALSE == ver_ptr_is_nio && NULL != ver_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, ver_ptr, _ver_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_max_report_size(short port, java.nio.ShortBuffer size_ptr)
 *     C function: ERRCODE_T tkcmdsetUSB_max_report_size(UINT16_T port, UINT16_T *  size_ptr);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1max_1report_1size1__SLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jshort port, jobject size_ptr, jint size_ptr_byte_offset, jboolean size_ptr_is_nio) {
  UINT16_T * _size_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != size_ptr ) {
    _size_ptr_ptr = (UINT16_T *) ( JNI_TRUE == size_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, size_ptr) :  (*env)->GetPrimitiveArrayCritical(env, size_ptr, NULL) );  }
  _res = tkcmdsetUSB_max_report_size((UINT16_T) port, (UINT16_T *) (((char *) _size_ptr_ptr) + size_ptr_byte_offset));
  if ( JNI_FALSE == size_ptr_is_nio && NULL != size_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, size_ptr, _size_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_read_flashblk16(short port, long address, java.nio.ShortBuffer val_ptr16, long len, java.nio.ShortBuffer packet_rsp_len)
 *     C function: ERRCODE_T tkcmdsetUSB_read_flashblk16(UINT16_T port, UINT32_T address, UINT16_T *  val_ptr16, UINT32_T len, UINT16_T *  packet_rsp_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1read_1flashblk161__SJLjava_lang_Object_2IZJLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jshort port, jlong address, jobject val_ptr16, jint val_ptr16_byte_offset, jboolean val_ptr16_is_nio, jlong len, jobject packet_rsp_len, jint packet_rsp_len_byte_offset, jboolean packet_rsp_len_is_nio) {
  UINT16_T * _val_ptr16_ptr = NULL;
  UINT16_T * _packet_rsp_len_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != val_ptr16 ) {
    _val_ptr16_ptr = (UINT16_T *) ( JNI_TRUE == val_ptr16_is_nio ?  (*env)->GetDirectBufferAddress(env, val_ptr16) :  (*env)->GetPrimitiveArrayCritical(env, val_ptr16, NULL) );  }
  if ( NULL != packet_rsp_len ) {
    _packet_rsp_len_ptr = (UINT16_T *) ( JNI_TRUE == packet_rsp_len_is_nio ?  (*env)->GetDirectBufferAddress(env, packet_rsp_len) :  (*env)->GetPrimitiveArrayCritical(env, packet_rsp_len, NULL) );  }
  _res = tkcmdsetUSB_read_flashblk16((UINT16_T) port, (UINT32_T) address, (UINT16_T *) (((char *) _val_ptr16_ptr) + val_ptr16_byte_offset), (UINT32_T) len, (UINT16_T *) (((char *) _packet_rsp_len_ptr) + packet_rsp_len_byte_offset));
  if ( JNI_FALSE == val_ptr16_is_nio && NULL != val_ptr16 ) {
    (*env)->ReleasePrimitiveArrayCritical(env, val_ptr16, _val_ptr16_ptr, 0);  }
  if ( JNI_FALSE == packet_rsp_len_is_nio && NULL != packet_rsp_len ) {
    (*env)->ReleasePrimitiveArrayCritical(env, packet_rsp_len, _packet_rsp_len_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_reset_port(short port)
 *     C function: ERRCODE_T tkcmdsetUSB_reset_port(UINT16_T port);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1reset_1port__S(JNIEnv *env, jobject _unused, jshort port) {
  ERRCODE_T _res;
  _res = tkcmdsetUSB_reset_port((UINT16_T) port);
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_send_command(short port, java.nio.ByteBuffer tx_ptr, short tx_len, short cmd_tag)
 *     C function: ERRCODE_T tkcmdsetUSB_send_command(UINT16_T port, UINT8_T *  tx_ptr, UINT16_T tx_len, UINT16_T cmd_tag);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1send_1command1__SLjava_lang_Object_2IZSS(JNIEnv *env, jobject _unused, jshort port, jobject tx_ptr, jint tx_ptr_byte_offset, jboolean tx_ptr_is_nio, jshort tx_len, jshort cmd_tag) {
  UINT8_T * _tx_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != tx_ptr ) {
    _tx_ptr_ptr = (UINT8_T *) ( JNI_TRUE == tx_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, tx_ptr) :  (*env)->GetPrimitiveArrayCritical(env, tx_ptr, NULL) );  }
  _res = tkcmdsetUSB_send_command((UINT16_T) port, (UINT8_T *) (((char *) _tx_ptr_ptr) + tx_ptr_byte_offset), (UINT16_T) tx_len, (UINT16_T) cmd_tag);
  if ( JNI_FALSE == tx_ptr_is_nio && NULL != tx_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, tx_ptr, _tx_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_serial_number(short port, java.nio.ByteBuffer ser_ptr, byte ser_len)
 *     C function: ERRCODE_T tkcmdsetUSB_serial_number(UINT16_T port, INT8_T *  ser_ptr, UINT8_T ser_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1serial_1number1__SLjava_lang_Object_2IZB(JNIEnv *env, jobject _unused, jshort port, jobject ser_ptr, jint ser_ptr_byte_offset, jboolean ser_ptr_is_nio, jbyte ser_len) {
  INT8_T * _ser_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != ser_ptr ) {
    _ser_ptr_ptr = (INT8_T *) ( JNI_TRUE == ser_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, ser_ptr) :  (*env)->GetPrimitiveArrayCritical(env, ser_ptr, NULL) );  }
  _res = tkcmdsetUSB_serial_number((UINT16_T) port, (INT8_T *) (((char *) _ser_ptr_ptr) + ser_ptr_byte_offset), (UINT8_T) ser_len);
  if ( JNI_FALSE == ser_ptr_is_nio && NULL != ser_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, ser_ptr, _ser_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_set_feature(short port, short feature, short timeout)
 *     C function: ERRCODE_T tkcmdsetUSB_set_feature(UINT16_T port, UINT16_T feature, UINT16_T timeout);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1set_1feature__SSS(JNIEnv *env, jobject _unused, jshort port, jshort feature, jshort timeout) {
  ERRCODE_T _res;
  _res = tkcmdsetUSB_set_feature((UINT16_T) port, (UINT16_T) feature, (UINT16_T) timeout);
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_shutdown(short port)
 *     C function: ERRCODE_T tkcmdsetUSB_shutdown(UINT16_T port);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1shutdown__S(JNIEnv *env, jobject _unused, jshort port) {
  ERRCODE_T _res;
  _res = tkcmdsetUSB_shutdown((UINT16_T) port);
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_shutdown_all()
 *     C function: ERRCODE_T tkcmdsetUSB_shutdown_all();
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1shutdown_1all__(JNIEnv *env, jobject _unused) {
  ERRCODE_T _res;
  _res = tkcmdsetUSB_shutdown_all();
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_special(short port, short cmd, long in_data, java.nio.LongBuffer out_data)
 *     C function: ERRCODE_T tkcmdsetUSB_special(UINT16_T port, UINT16_T cmd, UINT32_T in_data, UINT32_T *  out_data);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1special1__SSJLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jshort port, jshort cmd, jlong in_data, jobject out_data, jint out_data_byte_offset, jboolean out_data_is_nio) {
  UINT32_T * _out_data_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != out_data ) {
    _out_data_ptr = (UINT32_T *) ( JNI_TRUE == out_data_is_nio ?  (*env)->GetDirectBufferAddress(env, out_data) :  (*env)->GetPrimitiveArrayCritical(env, out_data, NULL) );  }
  _res = tkcmdsetUSB_special((UINT16_T) port, (UINT16_T) cmd, (UINT32_T) in_data, (UINT32_T *) (((char *) _out_data_ptr) + out_data_byte_offset));
  if ( JNI_FALSE == out_data_is_nio && NULL != out_data ) {
    (*env)->ReleasePrimitiveArrayCritical(env, out_data, _out_data_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_status(short port, java.nio.LongBuffer stat_ptr)
 *     C function: ERRCODE_T tkcmdsetUSB_status(UINT16_T port, UINT32_T *  stat_ptr);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1status1__SLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jshort port, jobject stat_ptr, jint stat_ptr_byte_offset, jboolean stat_ptr_is_nio) {
  UINT32_T * _stat_ptr_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != stat_ptr ) {
    _stat_ptr_ptr = (UINT32_T *) ( JNI_TRUE == stat_ptr_is_nio ?  (*env)->GetDirectBufferAddress(env, stat_ptr) :  (*env)->GetPrimitiveArrayCritical(env, stat_ptr, NULL) );  }
  _res = tkcmdsetUSB_status((UINT16_T) port, (UINT32_T *) (((char *) _stat_ptr_ptr) + stat_ptr_byte_offset));
  if ( JNI_FALSE == stat_ptr_is_nio && NULL != stat_ptr ) {
    (*env)->ReleasePrimitiveArrayCritical(env, stat_ptr, _stat_ptr_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_unregister_events()
 *     C function: ERRCODE_T tkcmdsetUSB_unregister_events();
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1unregister_1events__(JNIEnv *env, jobject _unused) {
  ERRCODE_T _res;
  _res = tkcmdsetUSB_unregister_events();
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_write_flashblk16(short port, long address, java.nio.ShortBuffer val_ptr16, long len, java.nio.ShortBuffer packet_rsp_len)
 *     C function: ERRCODE_T tkcmdsetUSB_write_flashblk16(UINT16_T port, UINT32_T address, UINT16_T *  val_ptr16, UINT32_T len, UINT16_T *  packet_rsp_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1write_1flashblk161__SJLjava_lang_Object_2IZJLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jshort port, jlong address, jobject val_ptr16, jint val_ptr16_byte_offset, jboolean val_ptr16_is_nio, jlong len, jobject packet_rsp_len, jint packet_rsp_len_byte_offset, jboolean packet_rsp_len_is_nio) {
  UINT16_T * _val_ptr16_ptr = NULL;
  UINT16_T * _packet_rsp_len_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != val_ptr16 ) {
    _val_ptr16_ptr = (UINT16_T *) ( JNI_TRUE == val_ptr16_is_nio ?  (*env)->GetDirectBufferAddress(env, val_ptr16) :  (*env)->GetPrimitiveArrayCritical(env, val_ptr16, NULL) );  }
  if ( NULL != packet_rsp_len ) {
    _packet_rsp_len_ptr = (UINT16_T *) ( JNI_TRUE == packet_rsp_len_is_nio ?  (*env)->GetDirectBufferAddress(env, packet_rsp_len) :  (*env)->GetPrimitiveArrayCritical(env, packet_rsp_len, NULL) );  }
  _res = tkcmdsetUSB_write_flashblk16((UINT16_T) port, (UINT32_T) address, (UINT16_T *) (((char *) _val_ptr16_ptr) + val_ptr16_byte_offset), (UINT32_T) len, (UINT16_T *) (((char *) _packet_rsp_len_ptr) + packet_rsp_len_byte_offset));
  if ( JNI_FALSE == val_ptr16_is_nio && NULL != val_ptr16 ) {
    (*env)->ReleasePrimitiveArrayCritical(env, val_ptr16, _val_ptr16_ptr, 0);  }
  if ( JNI_FALSE == packet_rsp_len_is_nio && NULL != packet_rsp_len ) {
    (*env)->ReleasePrimitiveArrayCritical(env, packet_rsp_len, _packet_rsp_len_ptr, 0);  }
  return _res;
}


/*   Java->C glue code:
 *   Java package: org.wmmnpr.c2.usb.impl.PM3USBCPImpl
 *    Java method: short tkcmdsetUSB_write_memblk16(short port, long address, java.nio.ShortBuffer val_ptr16, long len, java.nio.ShortBuffer packet_rsp_len)
 *     C function: ERRCODE_T tkcmdsetUSB_write_memblk16(UINT16_T port, UINT32_T address, UINT16_T *  val_ptr16, UINT32_T len, UINT16_T *  packet_rsp_len);
 */
JNIEXPORT jshort JNICALL 
Java_org_wmmnpr_c2_usb_impl_PM3USBCPImpl_tkcmdsetUSB_1write_1memblk161__SJLjava_lang_Object_2IZJLjava_lang_Object_2IZ(JNIEnv *env, jobject _unused, jshort port, jlong address, jobject val_ptr16, jint val_ptr16_byte_offset, jboolean val_ptr16_is_nio, jlong len, jobject packet_rsp_len, jint packet_rsp_len_byte_offset, jboolean packet_rsp_len_is_nio) {
  UINT16_T * _val_ptr16_ptr = NULL;
  UINT16_T * _packet_rsp_len_ptr = NULL;
  ERRCODE_T _res;
  if ( NULL != val_ptr16 ) {
    _val_ptr16_ptr = (UINT16_T *) ( JNI_TRUE == val_ptr16_is_nio ?  (*env)->GetDirectBufferAddress(env, val_ptr16) :  (*env)->GetPrimitiveArrayCritical(env, val_ptr16, NULL) );  }
  if ( NULL != packet_rsp_len ) {
    _packet_rsp_len_ptr = (UINT16_T *) ( JNI_TRUE == packet_rsp_len_is_nio ?  (*env)->GetDirectBufferAddress(env, packet_rsp_len) :  (*env)->GetPrimitiveArrayCritical(env, packet_rsp_len, NULL) );  }
  _res = tkcmdsetUSB_write_memblk16((UINT16_T) port, (UINT32_T) address, (UINT16_T *) (((char *) _val_ptr16_ptr) + val_ptr16_byte_offset), (UINT32_T) len, (UINT16_T *) (((char *) _packet_rsp_len_ptr) + packet_rsp_len_byte_offset));
  if ( JNI_FALSE == val_ptr16_is_nio && NULL != val_ptr16 ) {
    (*env)->ReleasePrimitiveArrayCritical(env, val_ptr16, _val_ptr16_ptr, 0);  }
  if ( JNI_FALSE == packet_rsp_len_is_nio && NULL != packet_rsp_len ) {
    (*env)->ReleasePrimitiveArrayCritical(env, packet_rsp_len, _packet_rsp_len_ptr, 0);  }
  return _res;
}


